package com.aerialdev.bigfoot;

public interface Callback {
    public int apply();
}
