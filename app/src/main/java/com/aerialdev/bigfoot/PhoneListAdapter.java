package com.aerialdev.bigfoot;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

public class PhoneListAdapter extends ArrayAdapter<PhoneData> {
    private final Context context;
    private final List<PhoneData> itemsList;

    public PhoneListAdapter(Context context, List<PhoneData> itemsList) {
        super(context, R.layout.phone_list_item, itemsList);

        this.context = context;
        this.itemsList = itemsList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.phone_list_item, parent, false);

        TextView labelView = (TextView) rowView.findViewById(R.id.label);
        TextView valueView = (TextView) rowView.findViewById(R.id.value);

        PhoneData phoneData = itemsList.get(position);
        labelView.setText(phoneData.getName());
        valueView.setText(String.format(Locale.US, "%.2f″, %.0fx%.0f pixels, %.0f ppi",
                phoneData.getInches(), phoneData.getyPx(), phoneData.getxPx(), phoneData.getPpi()));

        return rowView;
    }
}
