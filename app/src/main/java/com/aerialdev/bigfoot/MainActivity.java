package com.aerialdev.bigfoot;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private FloatingActionButton fab;
    private WebView browser;
    NavigationView navigationView;

    private boolean firstResume = false;
    private boolean isLarge = true;
    private int phoneListID = Integer.MAX_VALUE;
    private static final int RESULT_SETTINGS = 1;
    private RulerView ruler;
    private boolean includeBars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ChangeLog cl = new ChangeLog(this);
        if (cl.firstRun()) {
            cl.getLogDialog().show();
        }

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animateWebViewSize(fab);
                adjustFabPlacement();
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if(savedInstanceState == null){
            firstResume = true;
        }

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        generateNavigationWithData();

        prepareDrawerHeader();

        browser = (WebView) findViewById(R.id.webview);
        browser.getSettings().setDomStorageEnabled(true);
        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setLoadWithOverviewMode(true);
        browser.getSettings().setUseWideViewPort(true);
        browser.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        initSharedPreferencesData();
        adjustFabPlacement();

        updatePhoneDescriptionLabel();
    }

    private void generateNavigationWithData() {
        try {
            PhoneDataSupplier.setSelectedPhone(PhoneDataSupplier.getPhoneDataForNavi(this).get(0).getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        prepareNavigationView(navigationView, PhoneDataSupplier.getPhoneDataForNavi(this));
    }

    @Override
    protected void onPause() {
        super.onPause();
        PhoneDataSupplier.persistSelectedPhoneList(this);
    }

    private void initSharedPreferencesData() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        String url = getResources().getString(R.string.pref_url_default);
        String prefURL = fixUrl(sharedPrefs.getString("prefWebURL", getResources().getString(R.string.pref_url_default)));

        if(!url.equals(prefURL)) {
            url = prefURL;
        }
        try {
            browser.loadUrl(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ruler = (RulerView) findViewById(R.id.ruler);
        boolean showRuler = sharedPrefs.getBoolean("prefShowRuler", true);
        ruler.setVisibility(showRuler ? View.VISIBLE : View.INVISIBLE);

        includeBars = sharedPrefs.getBoolean("prefIncludeBars", true);
    }

    private void adjustFabPlacement(){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean showRuler = sharedPrefs.getBoolean("prefShowRuler", true);

        //No ruller? No right margin
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
        if(showRuler && !isLarge){
            layoutParams.rightMargin=getResources().getDimensionPixelSize(R.dimen.fab_margin_right_shifted);
        }else {
            layoutParams.rightMargin=getResources().getDimensionPixelSize(R.dimen.fab_margin_right);
        }
        fab.setLayoutParams(layoutParams);
    }

    private void updatePhoneDescriptionLabel() {
        PhoneData selectedPhone = PhoneDataSupplier.getSelectedPhone();

        TextView phone_label = (TextView) findViewById(R.id.content_phone_name_label);
        phone_label.setText(selectedPhone.getName());

        TextView phone_desc = (TextView) findViewById(R.id.content_phone_description_label);
        phone_desc.setText(String.format(Locale.US, "%.2f″, %.0fx%.0f pixels",
                selectedPhone.getInches(), selectedPhone.getyPx(), selectedPhone.getxPx()));
    }

    private void prepareDrawerHeader() {
        TextView screenInfoLabel= (TextView)findViewById(R.id.nav_header_screen_info);
        LocalDisplayMetrics dm = new LocalDisplayMetrics(getWindowManager().getDefaultDisplay(), this);

        double inches = dm.getInches();
        double ppi = dm.getPpi();
        double xPx = dm.getxPx();
        double yPx = dm.getyPx();

        screenInfoLabel.setText(String.format(Locale.US, "%.2f″, %.0fx%.0f pixels, %.0f ppi", inches, yPx, xPx, ppi));
    }

    private void prepareNavigationView(NavigationView navigationView, List<PhoneData> phoneData) {

        Menu m = navigationView.getMenu();
        m.removeItem(phoneListID);

        SubMenu topChannelMenu = m.addSubMenu(0, phoneListID, 0, R.string.phone_list_label);
        for(PhoneData pd : phoneData){
            topChannelMenu.add(0,pd.getId(),0,pd.getName() + " (" + pd.getInches()+"″)");
        }

        selectMenuItem(topChannelMenu.findItem(phoneData.get(0).getId()));

        MenuItem mi = m.getItem(m.size() - 1);
        mi.setTitle(mi.getTitle());

    }

    @Override
    protected void onResume() {
        //TODO - retrieve data from db
        super.onResume();

        long delay = 500;
        if(firstResume) {
            animateDrawerOpening();
            delay = 3000;
            firstResume = false;
        }

        browser.postDelayed(new Runnable() {
            @Override
            public void run() {
                animateWebViewSize(fab);
                adjustFabPlacement();
            }
        }, delay);
    }

    private void animateDrawerOpening() {
        drawer.postDelayed(new Runnable() {
            @Override
            public void run() {
                drawer.openDrawer(GravityCompat.START);
            }
        }, 1000);
        drawer.postDelayed(new Runnable() {
            @Override
            public void run() {
                drawer.closeDrawer(GravityCompat.START);
            }
        }, 2000);
    }

    private void animateWebViewSize(FloatingActionButton fab) {
        LocalDisplayMetrics dm = new LocalDisplayMetrics(getWindowManager().getDefaultDisplay(), this);

        PhoneData phoneData = PhoneDataSupplier.getSelectedPhone();
        double targetXInch = phoneData.getxPx() / phoneData.getPpi();
        double navBarSizes = includeBars ? 0-phoneData.getStatusBarHeight()-phoneData.getNavBarHeight() : 0;
        double targetYInch = (phoneData.getyPx()+navBarSizes)/ phoneData.getPpi();

        int browserHeight = browser.getHeight();
        double browserHeightInch = browserHeight/dm.getPpi();
        double newYRatio = targetYInch/browserHeightInch;
        int browserWidth = browser.getWidth();
        double browserWidthInch = browserWidth/dm.getPpi();
        double newXRatio = targetXInch/browserWidthInch;

        ResizeAnimation resizeAnimation = new ResizeAnimation(browser, newXRatio, newYRatio, isLarge);
        browser.startAnimation(resizeAnimation);
        isLarge = !isLarge;

        int drawableId = isLarge ? R.drawable.down: R.drawable.up;
        Drawable drawable = ContextCompat.getDrawable(MainActivity.this, drawableId);
        fab.setImageDrawable(drawable);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_other_phones) {
            openPhoneListDialog();
        } else if (id == R.id.nav_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult(i, RESULT_SETTINGS);
        } else if (id == R.id.nav_about) {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle(getString(R.string.about));
            alertDialog.setMessage(getString(R.string.about_message));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        } else {
            PhoneData phoneData = PhoneDataSupplier.getPhoneData(id);
            selectPhone(phoneData, new Callback() {
                @Override
                public int apply() {
                    //selectMenuItem(item);
                    return 1;
                }
            });
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openPhoneListDialog() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.phone_list);

        ListView lv = (ListView ) dialog.findViewById(R.id.lv);


        final List<PhoneData> itemsList = PhoneDataSupplier.getPhoneData();
        PhoneListAdapter listAdapter = new PhoneListAdapter(MainActivity.this, itemsList);
        lv.setAdapter(listAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        PhoneData phoneData = itemsList.get(position);

                selectPhone(phoneData, new Callback() {
                    @Override
                    public int apply() {
                        dialog.dismiss();
                        return 1;
                    }
                });
            }
        });

        dialog.show();
    }

    private void selectPhone(PhoneData phoneData, Callback callback) {
        if (phoneData != null) {
            Snackbar.make(browser, "Phone selected: " + phoneData.getName(), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            callback.apply();

            try {
                PhoneDataSupplier.setSelectedPhone(phoneData.getId());
            } catch (Exception e) {
                e.printStackTrace();
                Snackbar.make(browser, "A problem with selecting phone data occurred.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                return;
            }


            if (!isLarge) {
                animateWebViewSize(fab);
                adjustFabPlacement();
            }
            browser.postDelayed(new Runnable() {
                @Override
                public void run() {
                    animateWebViewSize(fab);
                    adjustFabPlacement();
                    updatePhoneDescriptionLabel();
                }
            }, 500);

            generateNavigationWithData();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SETTINGS:
                showUserSettings();
                break;

        }
    }

    private void showUserSettings() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        String url = fixUrl(sharedPrefs.getString("prefWebURL", getResources().getString(R.string.pref_url_default)));

        if(!browser.getUrl().equals(url)) {
            try {
                browser.loadUrl(url);
            } catch (Exception e) {
                e.printStackTrace();
                browser.loadUrl(getResources().getString(R.string.pref_url_default));
                Snackbar.make(browser, "A problem with opening selected url: "+url, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }

        boolean showRuler = sharedPrefs.getBoolean("prefShowRuler", true);
        ruler.setVisibility(showRuler ? View.VISIBLE : View.INVISIBLE);

        boolean newIncludeBars = sharedPrefs.getBoolean("prefIncludeBars", true);
        if(newIncludeBars != includeBars){
            includeBars=newIncludeBars;
            if (!isLarge) {
                animateWebViewSize(fab);
            }
            browser.postDelayed(new Runnable() {
                @Override
                public void run() {
                    animateWebViewSize(fab);
                }
            }, 500);
        }

        adjustFabPlacement();
    }

    private void selectMenuItem(MenuItem item) {
        Menu topMenu = navigationView.getMenu();
        Menu menu = topMenu.findItem(phoneListID).getSubMenu();

        for(int i = 0; i < menu.size(); i++) {
            MenuItem mItem = menu.getItem(i);
            if(item!=null && mItem.equals(item)){
                SpannableString spanString = new SpannableString(item.getTitle().toString());
                spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
                item.setTitle(spanString);
            }else{
                mItem.setTitle(mItem.getTitle().toString());
            }
        }

        //repaint
        MenuItem mi = topMenu.getItem(topMenu.size() - 1);
        mi.setTitle(mi.getTitle());
    }

    private String fixUrl(String url){
        if(!url.contains("http://")){
            url = "http://" + url;
        }
        return url;
    }
}
