package com.aerialdev.bigfoot;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;

public class LocalDisplayMetrics extends PhoneData {

    private Context context;

    public LocalDisplayMetrics(Display display, Context context) {
        this.context = context;
        android.util.DisplayMetrics dm = new android.util.DisplayMetrics();
        display.getMetrics(dm);

        int mWidthPixels = dm.widthPixels;
        int mHeightPixels = dm.heightPixels;

//         includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17)
        {
            try
            {
                mWidthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                mHeightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
            }
            catch (Exception ignored)
            {
            }
        }

        // includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 17)
        {
            try
            {
                Point realSize = new Point();
                Display.class.getMethod("getRealSize", Point.class).invoke(display, realSize);
                mWidthPixels = realSize.x;
                mHeightPixels= realSize.y;
            }
            catch (Exception ignored)
            {
            }
        }

        double x = Math.pow(mWidthPixels/dm.xdpi,2);
        double y = Math.pow(mHeightPixels/dm.ydpi,2);
        double screenInches = Math.sqrt(x+y);

        this.setxPx(mWidthPixels);
        this.setyPx(mHeightPixels);
        this.setPpi(Math.min(dm.xdpi, dm.ydpi));
        this.setNavBarHeight(calcNavBarHeight());
        this.setStatusBarHeight(calcStatusBarHeight());
        this.setInches(screenInches);
    }

    private int calcNavBarHeight(){
        Resources resources = this.context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    private int calcStatusBarHeight(){
        Resources resources = this.context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }
}
