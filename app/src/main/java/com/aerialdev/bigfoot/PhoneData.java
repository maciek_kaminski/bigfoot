package com.aerialdev.bigfoot;


public class PhoneData {
    private double inches;
    private double xPx;
    private double yPx;
    private double ppi;
    private String name;
    private String short_name;
    private int id;
    private int order;
    private int statusBarHeight = 0;
    private int navBarHeight = 0;

    public double getInches() {
        return inches;
    }

    public void setInches(double inches) {
        this.inches = inches;
    }

    public double getxPx() {
        return xPx;
    }

    public void setxPx(double xPx) {
        this.xPx = xPx;
    }

    public double getyPx() {
        return yPx;
    }

    public void setyPx(double yPx) {
        this.yPx = yPx;
    }

    public double getPpi() {
        return ppi;
    }

    public void setPpi(double ppi) {
        this.ppi = ppi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhoneData phoneData = (PhoneData) o;

        if (Double.compare(phoneData.inches, inches) != 0) return false;
        if (Double.compare(phoneData.xPx, xPx) != 0) return false;
        if (Double.compare(phoneData.yPx, yPx) != 0) return false;
        if (Double.compare(phoneData.ppi, ppi) != 0) return false;
        if (Integer.compare(phoneData.navBarHeight, navBarHeight) != 0) return false;
        if (Integer.compare(phoneData.statusBarHeight, statusBarHeight) != 0) return false;
        if (id != phoneData.id) return false;
        if (name != null ? !name.equals(phoneData.name) : phoneData.name != null) return false;
        return !(short_name != null ? !short_name.equals(phoneData.short_name) : phoneData.short_name != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(inches);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(xPx);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(yPx);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ppi);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (short_name != null ? short_name.hashCode() : 0);
        result = 31 * result + id;
        return result;
    }

    public void setStatusBarHeight(int statusBarHeight) {
        this.statusBarHeight = statusBarHeight;
    }

    public void setNavBarHeight(int navBarHeight) {
        this.navBarHeight = navBarHeight;
    }

    public int getStatusBarHeight() {
        return statusBarHeight;
    }

    public int getNavBarHeight() {
        return navBarHeight;
    }
}
