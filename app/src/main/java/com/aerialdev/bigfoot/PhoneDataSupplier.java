package com.aerialdev.bigfoot;


import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PhoneDataSupplier {
    private static List<PhoneData> dataMap = generatePhoneData();
    private static List<PhoneData> naviSelected = new ArrayList<>();
    private static Integer naviSelectedSize = 5;
    private static int selectedPhone;

    static PhoneDataSQLiteHelper db = null;


    private PhoneDataSupplier() throws Exception {
        throw new Exception();
    }

    public static PhoneData getPhoneData(int id){

        for(PhoneData pd: dataMap){
            if(pd.getId()==id){
                return pd;
            }
        }
        return null;
    }

    public static List<PhoneData> getPhoneData(){
        return dataMap;
    }

    public static List<PhoneData> getPhoneDataForNavi(Context ctx){
        if(db == null){
            db = new PhoneDataSQLiteHelper(ctx);
            naviSelected = db.getPhoneList();
        }

        if(naviSelected==null || naviSelected.isEmpty()) {
            naviSelected = new ArrayList<>(dataMap).subList(0, naviSelectedSize);
        }
        return naviSelected;
    }

    public static void persistSelectedPhoneList(Context ctx){
        if(db==null){
            db = new PhoneDataSQLiteHelper(ctx);
        }
        db.savePhoneList(naviSelected);
    }


    public static PhoneData getSelectedPhone() {
        return getPhoneData(selectedPhone);
    }

    public static void setSelectedPhone(int id) throws Exception {
        PhoneData phoneData = getPhoneData(id);
        if(phoneData==null){
            throw new Exception("Phone data does not exist in the database");
        }
        selectedPhone = id;

        adjustNaviList(phoneData);
    }

    private static void adjustNaviList(PhoneData phoneData) {
        naviSelected.remove(phoneData);
        naviSelected.add(0,phoneData);
        if (naviSelected.size()>naviSelectedSize){
            naviSelected = naviSelected.subList(0,naviSelectedSize);
        }
    }

    private static List<PhoneData> generatePhoneData() {

        PhoneData iphone5 = new PhoneData();
        iphone5.setInches(4);
        iphone5.setName("iPhone SE/5/5s/5c");
        iphone5.setShort_name("iphone5");
        iphone5.setId(1);
        iphone5.setxPx(640);
        iphone5.setyPx(1136);
        iphone5.setPpi(326);
        iphone5.setStatusBarHeight(40);
        iphone5.setNavBarHeight(0);

        PhoneData iphone6 = new PhoneData();
        iphone6.setInches(4.7);
        iphone6.setName("iPhone 6/6S");
        iphone6.setShort_name("iphone6");
        iphone6.setId(2);
        iphone6.setxPx(750);
        iphone6.setyPx(1334);
        iphone6.setPpi(326);
        iphone6.setStatusBarHeight(40);
        iphone6.setNavBarHeight(0);

        PhoneData iphone6plus = new PhoneData();
        iphone6plus.setInches(5.5);
        iphone6plus.setName("iPhone 6/6S Plus");
        iphone6plus.setShort_name("iphone6plus");
        iphone6plus.setId(3);
        iphone6plus.setxPx(1080);
        iphone6plus.setyPx(1920);
        iphone6plus.setPpi(401);
        iphone6plus.setStatusBarHeight(54);
        iphone6plus.setNavBarHeight(0);

        PhoneData me572c = new PhoneData();
        me572c.setInches(7);
        me572c.setId(4);
        me572c.setName("Asus MeMO Pad 7 (ME572C)");
        me572c.setShort_name("me572c");
        me572c.setxPx(1200);
        me572c.setyPx(1920);
        me572c.setPpi(323);
        me572c.setNavBarHeight(96);
        me572c.setStatusBarHeight(50);

        PhoneData h_p8 = new PhoneData();
        h_p8.setInches(5);
        h_p8.setId(5);
        h_p8.setName("Huawei P8");
        h_p8.setShort_name("h_p8");
        h_p8.setxPx(1080);
        h_p8.setyPx(1920);
        h_p8.setPpi(424);
        h_p8.setNavBarHeight(126);
        h_p8.setStatusBarHeight(75);

        PhoneData mate_8 = new PhoneData();
        mate_8.setInches(6);
        mate_8.setId(6);
        mate_8.setName("Huawei Mate 8");
        mate_8.setShort_name("mate_8");
        mate_8.setxPx(1080);
        mate_8.setyPx(1920);
        mate_8.setPpi(367);
        mate_8.setNavBarHeight(108);
        mate_8.setStatusBarHeight(75);
        //data from mate 7
        //? 144/75

        PhoneData lg_gpad = new PhoneData();
        lg_gpad.setInches(8.3);
        lg_gpad.setId(7);
        lg_gpad.setName("LG G Pad 8.3");
        lg_gpad.setShort_name("gpad");
        lg_gpad.setxPx(1200);
        lg_gpad.setyPx(1920);
        lg_gpad.setPpi(273);
        lg_gpad.setNavBarHeight(86);
        lg_gpad.setStatusBarHeight(50);


        PhoneData lumia = new PhoneData();
        lumia.setInches(5.7);
        lumia.setId(8);
        lumia.setxPx(1440);
        lumia.setName("Lumia 950XL");
        lumia.setShort_name("lumia");
        lumia.setyPx(2560);
        lumia.setPpi(518);
        lumia.setNavBarHeight(0);
        lumia.setStatusBarHeight(108);
        //2*status bar??

        PhoneData nexus_4 = new PhoneData();
        nexus_4.setInches(4.7);
        nexus_4.setId(9);
        nexus_4.setName("Nexus 4");
        nexus_4.setShort_name("nexus_4");
        nexus_4.setxPx(768);
        nexus_4.setyPx(1280);
        nexus_4.setPpi(318);
        nexus_4.setStatusBarHeight(50);
        nexus_4.setNavBarHeight(96);

        PhoneData nexus_5 = new PhoneData();
        nexus_5.setInches(5);
        nexus_5.setId(10);
        nexus_5.setName("Nexus 5");
        nexus_5.setShort_name("nexus_5");
        nexus_5.setxPx(1080);
        nexus_5.setyPx(1920);
        nexus_5.setPpi(445);
        nexus_5.setStatusBarHeight(75);
        nexus_5.setNavBarHeight(144);

        PhoneData n_6p = new PhoneData();
        n_6p.setInches(5.7);
        n_6p.setId(11);
        n_6p.setName("Nexus 6P");
        n_6p.setShort_name("n_6p");
        n_6p.setxPx(1440);
        n_6p.setyPx(2560);
        n_6p.setPpi(518);
        n_6p.setNavBarHeight(192);
        n_6p.setStatusBarHeight(96);
        //?

        PhoneData s6 = new PhoneData();
        s6.setInches(5.1);
        s6.setId(12);
        s6.setName("Samsung S6");
        s6.setShort_name("s6");
        s6.setxPx(1440);
        s6.setyPx(2560);
        s6.setPpi(577);
        s6.setNavBarHeight(0);
        s6.setStatusBarHeight(96);

        PhoneData s2_8 = new PhoneData();
        s2_8.setInches(8);
        s2_8.setId(13);
        s2_8.setName("Samsung Tab S2 8″");
        s2_8.setShort_name("s2_8");
        s2_8.setxPx(1536);
        s2_8.setyPx(2048);
        s2_8.setPpi(320);
        s2_8.setNavBarHeight(0);
        s2_8.setStatusBarHeight(50);

        PhoneData z5_c = new PhoneData();
        z5_c.setInches(4.6);
        z5_c.setId(14);
        z5_c.setName("Sony Xperia Z5 Compact");
        z5_c.setShort_name("z5_c");
        z5_c.setxPx(720);
        z5_c.setyPx(1280);
        z5_c.setPpi(319);
        z5_c.setNavBarHeight(96);
        z5_c.setStatusBarHeight(50);

        PhoneData xperia_z = new PhoneData();
        xperia_z.setInches(5);
        xperia_z.setId(15);
        xperia_z.setName("Sony Xperia Z");
        xperia_z.setShort_name("xperia_z");
        xperia_z.setxPx(1080);
        xperia_z.setyPx(1920);
        xperia_z.setPpi(443);
        xperia_z.setStatusBarHeight(75);
        xperia_z.setNavBarHeight(144);

        List<PhoneData> phoneList = new ArrayList<>();
        phoneList.add(iphone5);
        phoneList.add(iphone6);
        phoneList.add(iphone6plus);
        phoneList.add(h_p8);
        phoneList.add(mate_8);
        phoneList.add(nexus_4);
        phoneList.add(nexus_5);
        phoneList.add(n_6p);
        phoneList.add(lg_gpad);
        phoneList.add(lumia);
        phoneList.add(s6);
        phoneList.add(s2_8);
        phoneList.add(z5_c);
        phoneList.add(xperia_z);
        phoneList.add(me572c);


        Collections.sort(phoneList, new Comparator<PhoneData>() {
            @Override
            public int compare(PhoneData o1, PhoneData o2) {
                return ((Integer) o1.getId()).compareTo(o2.getId());
            }
        });

        return phoneList;
    }
}
