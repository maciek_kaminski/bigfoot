package com.aerialdev.bigfoot;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

public class PhoneDataSQLiteHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "BigFootPhoneDB";


    private static final String TABLE_PHONES = "phones";

    // Books Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_SHORT_NAME = "short_name";
    private static final String KEY_NAME = "name";
    private static final String KEY_INCHES = "inches";
    private static final String KEY_XPX = "xPx";
    private static final String KEY_YPX = "yPx";
    private static final String KEY_PPI = "ppi";
    private static final String KEY_NAV_BAR = "nav_bar";
    private static final String KEY_STATUS_BAR = "status_bar";
    private static final String KEY_ORDER = "order_key";

    private static final String[] COLUMNS = {KEY_ID, KEY_SHORT_NAME, KEY_NAME, KEY_INCHES, KEY_XPX, KEY_YPX, KEY_PPI, KEY_NAV_BAR, KEY_STATUS_BAR, KEY_ORDER};

    public PhoneDataSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create book table
        String CREATE_BOOK_TABLE = "CREATE TABLE "+TABLE_PHONES+" ( " +
                KEY_ID + " INTEGER PRIMARY KEY, " +
                KEY_SHORT_NAME + " TEXT, " +
                KEY_NAME + " TEXT, " +
                KEY_INCHES + " REAL, " +
                KEY_XPX + " REAL, " +
                KEY_YPX + " REAL, " +
                KEY_PPI + " REAL, " +
                KEY_STATUS_BAR + " INTEGER, " +
                KEY_NAV_BAR + " INTEGER, " +
                KEY_ORDER + " INTEGER )";

        // create books table
        db.execSQL(CREATE_BOOK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_PHONES);

        // create fresh books table
        this.onCreate(db);
    }

    public void savePhoneList(List<PhoneData> phoneList) {
        //for logging
        Log.d("savePhoneList", phoneList.toString());

        deletePhoneList();

        SQLiteDatabase db = this.getWritableDatabase();
        int order = 0;
        for (PhoneData pd : phoneList) {
            ContentValues values = new ContentValues();
            values.put(KEY_ID, pd.getId());
            values.put(KEY_SHORT_NAME, pd.getShort_name());
            values.put(KEY_NAME, pd.getName());
            values.put(KEY_INCHES, pd.getInches());
            values.put(KEY_XPX, pd.getxPx());
            values.put(KEY_YPX, pd.getyPx());
            values.put(KEY_PPI, pd.getPpi());
            values.put(KEY_STATUS_BAR, pd.getStatusBarHeight());
            values.put(KEY_NAV_BAR, pd.getNavBarHeight());

            values.put(KEY_ORDER, order);
            order++;

            db.insert(TABLE_PHONES, null, values);
        }

        db.close();
    }

    public List<PhoneData> getPhoneList() {
        List<PhoneData> phoneList = new LinkedList<>();

        String query = "SELECT  * FROM " + TABLE_PHONES + " ORDER BY " + KEY_ORDER;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        PhoneData pd = null;
        if (cursor.moveToFirst()) {
            do {
                pd = new PhoneData();
                pd.setId(Integer.parseInt(cursor.getString(0)));
                pd.setShort_name(cursor.getString(1));
                pd.setName(cursor.getString(2));
                pd.setInches(cursor.getDouble(3));
                pd.setxPx(cursor.getDouble(4));
                pd.setyPx(cursor.getDouble(5));
                pd.setPpi(cursor.getDouble(6));
                pd.setStatusBarHeight(cursor.getInt(7));
                pd.setNavBarHeight(cursor.getInt(8));

                phoneList.add(pd);
            } while (cursor.moveToNext());
        }

        Log.d("getPhoneList()", phoneList.toString());

        return phoneList;
    }

    public void deletePhoneList() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + TABLE_PHONES);

        Log.d("deletePhoneList", "from " + TABLE_PHONES);
    }

}

