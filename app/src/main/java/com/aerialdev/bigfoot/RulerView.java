package com.aerialdev.bigfoot;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;

public class RulerView extends View{

    public static final float INCH_2_MM = 25.4f;

    static int screenSize = 666;
    static private float pxmm = screenSize / 67.f;
    static private float yppi = 0;
    static private float xppi = 0;
    int width, height, midScreenPoint;
    float startingPoint = 0;
    float downpoint = 0, movablePoint = 0, downPointClone = 0;
    private float mainPoint = 0, mainPointClone = 0;
    boolean isDown = false;
    boolean isUpward = false;
    private boolean isMove;
    private Paint gradientPaint;
    private float rulersize = 0;
    private Paint rulerPaint, textPaint, goldenPaint;
    private int endPoint;
    boolean isSizeChanged = false;
    float userStartingPoint = 0f;
    private int scaleLineSmall;
    private int scaleLineMedium;
    private int scaleLineLarge;
    private int textStartPoint;
    private int yellowLineStrokeWidth;
    boolean isFirstTime = true;

    public RulerView(Context context) {
        super(context);
    }

    public RulerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init(context);
        }
    }

    @Override
    public void onSizeChanged(int w, int h, int oldW, int oldH) {
        width = w;
        height = h;
        screenSize = height;
//        pxmm = screenSize / 67.f;
        float eee = screenSize / 67.f;
        pxmm = yppi/ INCH_2_MM;
        midScreenPoint = height / 2;
        endPoint = width - 20;
        if (isSizeChanged) {
            isSizeChanged = false;
            mainPoint = midScreenPoint - (userStartingPoint * 10 * pxmm);
        }
    }

    private void init(Context context) {
        Activity host = (Activity) context;
        Display display = host.getWindowManager().getDefaultDisplay();
        android.util.DisplayMetrics dm = new android.util.DisplayMetrics();
        display.getMetrics(dm);

        screenSize = dm.heightPixels;
        yppi = dm.ydpi;
        xppi = dm.xdpi;

        //gradientPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        rulersize = pxmm * 10;
        rulerPaint = new Paint();
        rulerPaint.setStyle(Paint.Style.STROKE);
        rulerPaint.setStrokeWidth(0);
        rulerPaint.setAntiAlias(false);
        rulerPaint.setColor(Color.BLACK);
        textPaint = new TextPaint();
        //Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/segoeuil.ttf");
        // textPaint.setTypeface(typeface);
        textPaint.setStyle(Paint.Style.STROKE);
        textPaint.setStrokeWidth(0);
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(16);
        textPaint.setColor(Color.BLACK);
        scaleLineSmall = 10;
        scaleLineMedium = 25;
        scaleLineLarge = 50;
        textStartPoint = 107;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        startingPoint = screenSize;
        for (int i = 1;; ++i) {
            if (startingPoint < 0) {
                break;
            }
            startingPoint = startingPoint - pxmm;
            int size = (i % 10 == 0) ? scaleLineLarge : (i % 5 == 0) ? scaleLineMedium : scaleLineSmall;
            canvas.drawLine(endPoint - size, startingPoint, endPoint, startingPoint, rulerPaint);
            if (i % 10 == 0) {
                System.out.println("done   " + i);
                canvas.drawText((i / 10) + " cm", endPoint - textStartPoint, startingPoint + 8, textPaint);
            }
        }

    }

}
