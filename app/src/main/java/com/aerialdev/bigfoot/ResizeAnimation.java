package com.aerialdev.bigfoot;


import android.support.percent.PercentRelativeLayout;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class ResizeAnimation extends Animation {
    private View mView;
    private double mToHeight;
    private double mFromHeight;

    private double mToWidth;
    private double mFromWidth;

    public ResizeAnimation(View v, double smallWidth, double smallHeight, boolean isLarge) {
        if(!isLarge){
            mToHeight = 1;
            mToWidth = 1;
            mFromHeight = smallHeight;
            mFromWidth = smallWidth;
        }else{
            mToHeight = smallHeight;
            mToWidth = smallWidth;
            mFromHeight = 1;
            mFromWidth = 1;
        }
        mView = v;
        setDuration(300);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        double height =
                (mToHeight - mFromHeight) * interpolatedTime + mFromHeight;
        double width = (mToWidth - mFromWidth) * interpolatedTime + mFromWidth;
        PercentRelativeLayout.LayoutParams p = (PercentRelativeLayout.LayoutParams) mView.getLayoutParams();
        p.getPercentLayoutInfo().heightPercent = (float) height;
        p.getPercentLayoutInfo().widthPercent = (float) width;
        mView.setLayoutParams(p);
    }
}